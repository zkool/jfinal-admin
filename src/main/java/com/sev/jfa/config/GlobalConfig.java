package com.sev.jfa.config;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.plugin.sqlinxml.SqlInXmlPlugin;
import com.jfinal.ext.plugin.tablebind.AutoTableBindPlugin;
import com.jfinal.ext.route.AutoBindRoutes;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.SqlReporter;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.sev.jfa.common.CaseCamelContainerFactory;

/**
 * @author kool.zhao
 *
 */
public class GlobalConfig extends JFinalConfig{
	
	private boolean isDev;
	private Routes routes;
	
	private boolean isDevMode(){
		String osName = System.getProperty("os.name");
		return osName.indexOf("Windows") != -1;
	}
	
	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("classes/db.properties");
		isDev = getPropertyToBoolean("devMode", false);
		me.setDevMode(isDev);
		me.setBaseViewPath("/WEB-INF/view");
	}

	/* (non-Javadoc) 配置路由
	 * @see com.jfinal.config.JFinalConfig#configRoute(com.jfinal.config.Routes)
	 */
	@Override
	public void configRoute(Routes me) {
		this.routes = me;
		me.add(new AutoBindRoutes());
	}

	@Override
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
//		C3p0Plugin dbPlugin = new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password").trim());
		
		// 配置Druid 数据库连接池插件
		DruidPlugin dbPlugin = new DruidPlugin(getProperty("jdbcUrl"),getProperty("user"),getProperty("password"));
		 // 设置 状态监听与 sql防御
		WallFilter wall = new WallFilter();
		wall.setDbType(getProperty("dbType"));
		dbPlugin.addFilter(wall);
		dbPlugin.addFilter(new StatFilter());
		
		me.add(dbPlugin);
		
		// add EhCache
		me.add(new EhCachePlugin());
		// add sql xml plugin
		me.add(new SqlInXmlPlugin());
		// add shrio
//		if (Consts.OPEN_SHIRO) me.add(new ShiroPlugin(this.routes));
				
		// 配置ActiveRecord插件
//		ActiveRecordPlugin arp = new ActiveRecordPlugin(dbPlugin);
		
		// 配置AutoTableBindPlugin插件
		AutoTableBindPlugin atbp = new AutoTableBindPlugin(dbPlugin);
		atbp.addExcludeClasses(com.jfinal.ext.kit.ModelExt.class);
		if(isDev) atbp.setShowSql(true);
		atbp.setContainerFactory(new CaseCamelContainerFactory());
//		atbp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
		me.add(atbp);
	}

	@Override
	public void configInterceptor(Interceptors me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler());
	}

	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		JFinal.start("src/main/webapp", 80, "/", 5);
	}
}
