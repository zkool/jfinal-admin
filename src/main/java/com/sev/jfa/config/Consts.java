package com.sev.jfa.config;

public class Consts
{

	/***
	 * shiro 权限 管理 开关
	 */
	public static boolean OPEN_SHIRO = true;

	/***
	 * 分布式session开关 请在redis.properties 配置ip和端口
	 */
	public static boolean OPEN_REDIS = true;

	public static boolean ALIYUN_DB = true;

	/**
	 * 动态查询，参数前缀
	 */
	public final static String SEARCH_PREFIX = "search_";
}
