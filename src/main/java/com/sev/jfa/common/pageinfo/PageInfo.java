package com.sev.jfa.common.pageinfo;


/**
 * @author kool.zhao
 *
 */
public class PageInfo {

	 public static final int DEFAULT_PAGE_SIZE = 10;
		
	 private int pageNumber;

	 private int pageSize;

	 private String sorterField;

	 private String sorterDirection;
	  
	 private String sqlExceptSelect;
	 
	 private Object[] paras;

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSorterField() {
		return sorterField;
	}

	public void setSorterField(String sorterField) {
		this.sorterField = sorterField;
	}

	public String getSorterDirection() {
		return sorterDirection;
	}

	public void setSorterDirection(String sorterDirection) {
		this.sorterDirection = sorterDirection;
	}

	public String getSqlExceptSelect() {
		return sqlExceptSelect;
	}

	public void setSqlExceptSelect(String sqlExceptSelect) {
		this.sqlExceptSelect = sqlExceptSelect;
	}

	public Object[] getParas() {
		return paras;
	}

	public void setParas(Object[] paras) {
		this.paras = paras;
	}

}
