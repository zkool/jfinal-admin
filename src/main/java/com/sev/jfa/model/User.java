package com.sev.jfa.model;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@TableBind(tableName = "system_user")
public class User extends Model<User>{

	private static final long serialVersionUID = 6195101631553726552L;
	
	public static final User dao = new User();
	
	
	public Page<User> paginate(int pageNumber, int pageSize,String sqlExceptSelect,Object[] paras) {
		return paginate(pageNumber, pageSize, "select *", sqlExceptSelect,paras);
	}
}
