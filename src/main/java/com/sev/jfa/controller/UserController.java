package com.sev.jfa.controller;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.sev.jfa.common.pageinfo.DynamicPageInfo;
import com.sev.jfa.common.pageinfo.PageInfo;
import com.sev.jfa.model.User;

@ControllerBind(controllerKey="system/user")
public class UserController extends Controller{

	public void list(){
		PageInfo pageInfo = DynamicPageInfo.bySearchFilter(getRequest(), User.class);
		setAttr("userPage",User.dao.paginate(getParaToInt(0, 1), 10, pageInfo.getSqlExceptSelect(),pageInfo.getParas()));
		render("list.html");
	}
	
	public void add(){
		
	}
	
	public void save(){
		getModel(User.class).save();
		redirect("list");
	}
	
	public void edit(){
		setAttr("user",User.dao.findById(getParaToInt()));
	}
	
	public void update() {
		getModel(User.class).update();
		redirect("list");
	}
	
	public void delete() {
		User.dao.deleteById(getParaToInt());
		redirect("list");
	}
}
