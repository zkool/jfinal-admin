package com.sev.jfa.controller;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey="/")
public class IndexController extends Controller{

	public void index(){
		render("index/index.html");
	}
	
	public void main(){
		render("index/main.html");
	}
}
